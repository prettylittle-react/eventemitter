class EventEmitter {
	constructor() {
		if (SERVER) {
			return;
		}

		if (typeof window._Events === 'undefined') {
			window._events = {};
		}
	}

	subscribe(event, callback) {
		if (SERVER) {
			return;
		}

		if (!window._events[event]) {
			window._events[event] = [];
		}

		window._events[event].push(callback);
	}

	unsubscribe(event, callback = null) {
		if (SERVER) {
			return;
		}

		if (!window._events[event]) {
			return;
		}

		if (callback) {
			// TODO: better way of removing the item
			for (let i = 0; i < window._events[event].length; i++) {
				if (window._events[event][i] === callback) {
					window._events[event][i] = null;

					return;
				}
			}

			return;
		}

		delete window._events[event];
	}

	dispatch(event, ...args) {
		if (SERVER) {
			return;
		}

		if (!window._events[event]) {
			return;
		}

		for (let i = 0; i < window._events[event].length; i++) {
			if (window._events[event][i]) {
				window._events[event][i](...args);
			}
		}
	}
}

export default new EventEmitter();
